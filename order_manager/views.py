from datetime import datetime

from django.db.models import Sum, F, Case, When
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet
from order_manager.models import Product, Inventory, Order, OrderItem
from order_manager.serializers import ProductSerializer, InventorySerializer, OrderSerializer, OrderItemSerializer

# Create your views here.

class InventoryViewSet(ModelViewSet):
    queryset = Inventory.objects.all()
    serializer_class = InventorySerializer

    def get_queryset(self):
        queryset = super().get_queryset()
        if self.request.query_params.get('date'):
            queryset = queryset.filter(date=self.request.query_params.get('date'))
        return queryset

    @action(methods=['POST'], detail=False, url_path='calculate-inventory')
    def calculate_inventory(self, *args, **kwargs):
        date = datetime.strptime(self.request.data['date'], '%Y-%m-%d').date()
        Inventory.calculate_inventory(date)
        inventories = Inventory.objects.filter(date=date)
        serializer = self.serializer_class(inventories, many=True)
        return Response(serializer.data, status=200)


class OrderViewSet(ModelViewSet):
    queryset = Order.objects.all()
    serializer_class = OrderSerializer


class OrderItemViewSet(ModelViewSet):
    queryset = OrderItem.objects.all()
    serializer_class = OrderItemSerializer


class ProductViewSet(ModelViewSet):
    queryset = Product.objects.annotate(total_quantity=Sum('orderitem__quantity'))
    serializer_class = ProductSerializer

    def get_queryset(self):
        queryset = super().get_queryset()
        if self.request.query_params.get('date'):
            ids = list(queryset.filter(orderitem__order__delivery_date=self.request.query_params.get('date')).values_list('id', flat=True))
            queryset = queryset.filter(id__in=ids)

        if self.request.query_params.get('more_sale') == 'true':
            return queryset.order_by('-total_quantity').filter(total_quantity__gte=0)

        if self.request.query_params.get('less_sale') == 'true':
            return queryset.order_by('total_quantity').filter(total_quantity__gte=0)

        if self.request.query_params.get('order_id'):
            queryset = Product.objects.all()
            queryset = queryset.filter(orderitem__order__id=self.request.query_params.get('order_id'))\
                .annotate(
                product_to_order=F('orderitem__quantity'),
                product_in_inventory=F('inventory__quantity'),
                product_to_provider=Case(
                    When(inventory__quantity__lte=0, then=F('orderitem__quantity')),
                    When(inventory__quantity__gte=F('orderitem__quantity'), then=0),
                    When(orderitem__quantity__gt=F('inventory__quantity'), then=(F('inventory__quantity') - F('orderitem__quantity'))*-1
                         )))

        return queryset
