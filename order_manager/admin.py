from django.contrib import admin

# Register your models here.
from order_manager.models import Product, Order, OrderItem, Inventory, Provider


class ProductAdmin(admin.ModelAdmin):
    list_display = ('code', 'name')


class OrderAdmin(admin.ModelAdmin):
   list_display = ('id', 'priority', 'address', 'user', 'delivery_date')


class OrderItemAdmin(admin.ModelAdmin):
    list_display = ('quantity', 'product', 'order')
    raw_id_fields = ('product', 'order')

class InventoryAdmin(admin.ModelAdmin):
    list_display = ('quantity', 'product', 'date')

class ProviderAdmin(admin.ModelAdmin):
    list_display = ('nit', 'name', 'product')


admin.site.register(Product, ProductAdmin)
admin.site.register(Order, OrderAdmin)
admin.site.register(OrderItem, OrderItemAdmin)
admin.site.register(Inventory, InventoryAdmin)
admin.site.register(Provider, ProviderAdmin)
