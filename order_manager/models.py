import datetime
from datetime import timedelta
from xmlrpc.client import DateTime

from django.db import models

# Create your models here.


class Product(models.Model):
    code = models.IntegerField()
    name = models.CharField(max_length=150)

    def __str__(self):
        return u'{} {}'.format(self.name, self.code)


class Order(models.Model):
    priority = models.IntegerField()
    address = models.CharField(max_length=150)
    user = models.CharField(max_length=150)
    delivery_date = models.DateField()

    def __str__(self):
        return u'{} {}'.format(self.user, self.priority)


class OrderItem(models.Model):
    quantity = models.IntegerField()
    product = models.ForeignKey(Product, on_delete=models.PROTECT)
    order = models.ForeignKey(Order, on_delete=models.PROTECT)

    def __str__(self):
        return u'{} {}'.format(self.product.name, self.order.user)


class Inventory(models.Model):
    product = models.ForeignKey(Product, on_delete=models.PROTECT)
    quantity = models.IntegerField()
    date = models.DateField()

    def __str__(self):
        return u'{} {} {}'.format(self.product.name, self.quantity, self.date)

    @classmethod
    def calculate_inventory(cls, date: datetime.date):

        day_before = date - timedelta(days=1)
        order_items = OrderItem.objects.filter(order__delivery_date=day_before)

        for order_item in order_items:
            inventory = Inventory.objects.filter(date=day_before, product=order_item.product).last()
            if inventory:
                Inventory.objects.update_or_create(date=date, product=inventory.product, defaults={'quantity': inventory.quantity - order_item.quantity})




class Provider(models.Model):
    nit = models.IntegerField()
    name = models.CharField(max_length=150)
    product = models.ForeignKey(Product, on_delete=models.PROTECT)

    def __str__(self):
        return u'{} {} {}'.format(self.nit, self.name, self.product)

    class Meta:
        unique_together = [['nit', 'product']]
