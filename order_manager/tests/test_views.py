from django.contrib.auth import get_user_model
from django.test.testcases import TestCase, Client

from order_manager.models import Product, Order, Inventory, OrderItem


class ProductViewSetTest(TestCase):
    def setUp(self):
        User = get_user_model()
        self.user = User.objects.create(username='kevin', password='kevin,', is_staff=True, is_superuser=True)
        self.product1 = Product.objects.create(**{
            'code': '1',
            'name': 'papa'
        })

        self.product2 = Product.objects.create(**{
            'code': '2',
            'name': 'arroz'
        })

        self.order1 = create_order('1', 'carrera 1 #1', 'Kevin', '2019-03-01')
        self.order2 = create_order('2', 'carrera 2 #2', 'Dayana', '2019-03-02')
        self.order3 = create_order('3', 'carrera 3 #3', 'Ana', '2019-03-02')
        self.order4 = create_order('3', 'carrera 3 #3', 'Ana', '2019-03-03')

        self.order_item1 = create_order_item('2', self.product1, self.order1)
        self.order_item2 = create_order_item('2', self.product1, self.order2)
        self.order_item2 = create_order_item('2', self.product2, self.order3)

        self.inventory1 = create_inventory(self.product1, '0', '2019-03-01')

        self.client = Client()
        self.client.force_login(self.user)

    def test_product_list_is_working(self):
        path = '/order_manager/product/'
        response = self.client.get(path)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(self.product1.id, response.data[1]['id'])

    def test_product_filter_by_date(self):
        path = '/order_manager/product/?date=2019-03-01'
        response = self.client.get(path)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(1, len(response.data))

    def test_product_filter_by_date_2_records(self):
        path = '/order_manager/product/?date=2019-03-02'
        response = self.client.get(path)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(2, len(response.data))

    def test_product_filter_by_date_empty_list(self):
        path = '/order_manager/product/?date=2019-03-03'
        response = self.client.get(path)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(0, len(response.data))

    def test_product_more_sale(self):
        path = '/order_manager/product/?more_sale=true'
        response = self.client.get(path)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(4, response.data[0]['total_quantity'])

    def test_product_less_sale(self):
        path = '/order_manager/product/?less_sale=true'
        response = self.client.get(path)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(2, response.data[0]['total_quantity'])

    def test_product_by_order(self):
        path = '/order_manager/product/?order_id=1'
        response = self.client.get(path)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(2, response.data[0]['product_to_provider'])


class InventoryViewSetTest(TestCase):
    def setUp(self):
        User = get_user_model()
        self.user = User.objects.create(username='kevin', password='kevin,', is_staff=True, is_superuser=True)
        self.product1 = Product.objects.create(**{
            'code': '1',
            'name': 'papa'
        })

        self.product2 = Product.objects.create(**{
            'code': '2',
            'name': 'arroz'
        })

        self.inventory1 = create_inventory(self.product1, '0', '2019-03-01')
        self.inventory1 = create_inventory(self.product2, '0', '2019-03-01')
        self.inventory1 = create_inventory(self.product1, '0', '2019-03-02')

        self.client = Client()
        self.client.force_login(self.user)

    def test_inventory_filter_by_date(self):
        path = '/order_manager/inventory/?date=2019-03-02'
        response = self.client.get(path)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(1, len(response.data))

    def test_inventory_filter_by_date_2_records(self):
        path = '/order_manager/inventory/?date=2019-03-01'
        response = self.client.get(path)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(2, len(response.data))


def create_order(priority, address, user, delivery_date):
    return Order.objects.create(**{
        'priority': priority,
        'address': address,
        'user': user,
        'delivery_date':delivery_date
    })

def create_order_item(quantity, product, order):
    return OrderItem.objects.create(**{
        'quantity': quantity,
        'product': product,
        'order': order
    })

def create_inventory(product, quantity, date):
    return Inventory.objects.create(**{
        'product': product,
        'quantity': quantity,
        'date': date
    })