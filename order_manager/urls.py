from django.urls import re_path, include, path
from rest_framework import routers
from order_manager.views import InventoryViewSet, OrderViewSet, OrderItemViewSet, ProductViewSet

router = routers.DefaultRouter()

router.register('inventory', InventoryViewSet)
router.register('order', OrderViewSet)
router.register('order-item', OrderItemViewSet)
router.register('product', ProductViewSet)

urlpatterns = [
    re_path('', include(router.urls)),
]
