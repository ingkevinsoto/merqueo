from rest_framework import serializers

from order_manager.models import Product, Inventory, Order, OrderItem


class ProductSerializer(serializers.ModelSerializer):
    total_quantity = serializers.IntegerField(default=0)
    product_to_order = serializers.IntegerField(default=0)
    product_in_inventory = serializers.IntegerField(default=0)
    product_to_provider = serializers.IntegerField(default=0)

    class Meta:
        model = Product
        fields = '__all__'


class InventorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Inventory
        fields = '__all__'


class OrderSerializer(serializers.ModelSerializer):
    class Meta:
        model = Order
        fields = '__all__'


class OrderItemSerializer(serializers.ModelSerializer):
    order = OrderSerializer(many=False)
    product = ProductSerializer(many=False)
    class Meta:
        model = OrderItem
        fields = '__all__'
